Imports System.Xml
Imports System.Text
Imports System.IO

Public Class MakeAutonomyIDX
    Public xmlDoc As XmlDocument
    Public sw As StreamWriter

    Public Sub Open(ByVal strIdxFile As String)
        sw = New StreamWriter(strIdxFile, False, System.Text.Encoding.GetEncoding(1252))   ' create a stream writer 
    End Sub

    Public Sub Close()
        xmlDoc = Nothing
        sw.Flush()
        sw.Close()
    End Sub

    Public Function NewIdx(ByVal strDbName As String, ByRef strXml As String) As Boolean

        Dim intArticleId As Integer
        Dim CreationDateTime As DateTime
        Dim ArticleTitle As String
        Dim Ingress As String
        Dim strMaingroup As String
        Dim strSubgroup As String
        Dim intNumRows As Integer
        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim strAutonomyEtc As String

        xmlDoc.LoadXml(strXml)
        strMaingroup = xmlDoc.SelectSingleNode("nitf/head/tobject/@tobject.type").InnerText
        strSubgroup = xmlDoc.SelectSingleNode("nitf/head/tobject/tobject.property/@tobject.property.type").InnerText

        strAutonomyEtc = ""
        strAutonomyEtc &= xmlDoc.SelectSingleNode("nitf/head/docdata/evloc").InnerText & "; "
        strAutonomyEtc &= xmlDoc.SelectSingleNode("nitf/head/docdata/evloc/@county-dist").InnerText

        GetCategories(xmlDoc, strAutonomyEtc)

        'Autonomy IDX-file
        Dim strIDX As String
        strIDX = "#DREREFERENCE " & Trim(intArticleId.ToString) & vbCrLf
        strIDX &= "#DRETITLE " & ArticleTitle & vbCrLf
        strIDX &= "#DREFIELD summary=""" & Ingress.Replace("""", "&quot;") & """" & vbCrLf

        strIDX &= "#DREFIELD Maingroup=""" & strMaingroup & """" & vbCrLf
        strIDX &= "#DREFIELD Subgroup=""" & strSubgroup & """" & vbCrLf
        'strIDX &= "#DREFIELD Categories=""" & intCategories & """" & vbCrLf
        strIDX &= "#DREFIELD CreationDateTime=""" & CreationDateTime & """" & vbCrLf

        Dim DreDate As String = Format(CreationDateTime, "yyyy/MM/dd/HH/mm").Replace(".", "/")
        strIDX &= "#DREFIELD DREDATE=""" & DreDate & """" & vbCrLf

        strIDX &= "#DREDBNAME " & strDbName & vbCrLf
        strIDX &= "#DRESTORECONTENT y" & vbCrLf
        strIDX &= "#DRECONTENT" & vbCrLf

        'strIDX &= strMaingroup & "; " & strSubgroup & "; " & strAutonomyEtc & vbCrLf

        Dim xmlNodeListBody As XmlNodeList = xmlDoc.SelectNodes("nitf/body/body.content/p")
        Dim xmlPar As XmlNode
        Dim strBody As String = ""
        For Each xmlPar In xmlNodeListBody
            If Trim(xmlPar.InnerText) <> "" Then
                strBody &= xmlPar.InnerText & vbCrLf
            End If
        Next

        If strBody = "" Then
            strBody = xmlDoc.SelectSingleNode("nitf/body/body.content").InnerText
        End If

        strIDX &= strBody

        strIDX &= "#DREENDDOC" & vbCrLf

        sw.WriteLine(strIDX)
    End Function

    Private Function GetCategories(ByRef xmlDoc As XmlDocument, ByRef strAutonomyEtc As String) As Boolean
        '*** Get the String values for Categories and Subcat to strAutonomyEtc
        Dim strNITFContent As String
        Dim strSubcat As String
        Dim strSubcatList As String
        Dim strNITFContDone As String = ""
        Dim i As Integer

        Dim node As XmlNode

        For Each node In xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject")
            strNITFContent = node.Attributes.GetNamedItem("tobject.subject.code").InnerText
            If strNITFContDone.IndexOf(strNITFContent) = -1 Then
                strNITFContDone &= strNITFContent & "; "

                Try
                    strAutonomyEtc &= node.Attributes.GetNamedItem("tobject.subject.type").InnerText & "; " 'For Autonumy
                Catch
                End Try

                Try
                    strSubcatList = node.Attributes.GetNamedItem("tobject.subject.matter").InnerText
                    strAutonomyEtc &= strSubcatList 'For Autonumy
                Catch
                    'intHasReferences = 0
                End Try
            End If
        Next

        Return True
    End Function

End Class
