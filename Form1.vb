Imports System.Xml
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        Me.ComboBox1.Text = "NTB"
        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents btnStartExport As System.Windows.Forms.Button
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents SqlDataAdapter1 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DataSet11 As NTB_Export_to_Autonomy_IDX.DataSet1
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.btnStartExport = New System.Windows.Forms.Button()
        Me.StatusBar1 = New System.Windows.Forms.StatusBar()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.SqlDataAdapter1 = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.DataSet11 = New NTB_Export_to_Autonomy_IDX.DataSet1()
        CType(Me.DataSet11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "data source=PORTALSERVER;initial catalog=ntb_archive;password=password;persist se" & _
        "curity info=True;user id=sa;workstation id=GX260-LCH;packet size=4096;Timeout=12" & _
        "0"
        '
        'btnStartExport
        '
        Me.btnStartExport.Location = New System.Drawing.Point(24, 72)
        Me.btnStartExport.Name = "btnStartExport"
        Me.btnStartExport.Size = New System.Drawing.Size(120, 23)
        Me.btnStartExport.TabIndex = 0
        Me.btnStartExport.Text = "Start Export"
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 327)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Size = New System.Drawing.Size(432, 22)
        Me.StatusBar1.TabIndex = 1
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"NTB", "NTB_Archive"})
        Me.ComboBox1.Location = New System.Drawing.Point(24, 40)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(128, 21)
        Me.ComboBox1.TabIndex = 2
        '
        'SqlDataAdapter1
        '
        Me.SqlDataAdapter1.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ARTICLES", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefID", "RefID"), New System.Data.Common.DataColumnMapping("ArticleXML", "ArticleXML")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT RefID, ArticleXML FROM ARTICLES"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'DataSet11
        '
        Me.DataSet11.DataSetName = "DataSet1"
        Me.DataSet11.Locale = New System.Globalization.CultureInfo("nb-NO")
        Me.DataSet11.Namespace = "http://www.tempuri.org/DataSet1.xsd"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(432, 349)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.ComboBox1, Me.StatusBar1, Me.btnStartExport})
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.DataSet11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnStartExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartExport.Click
        StartExport()
    End Sub

    Private Sub StartExport()
        Dim strSqlWhere As String
        Dim bArkiv As Boolean
        Dim strConnectionString As String

        Dim strDbName As String = Me.ComboBox1.Text
        strSqlWhere = AppSettings("SqlQueryWhere" & strDbName)

        Dim strQuery As String = "SELECT RefID, ArticleXML FROM ARTICLES " _
            & strSqlWhere

        Dim fileIdx As AutonomyIDX = New AutonomyIDX(AppSettings("ConnectionString"))

        If strDbName = "NTB" Then
            bArkiv = False
            strConnectionString = AppSettings("ConnectionString")
            MakeIdxFile(strConnectionString, strQuery, fileIdx, "NTB", bArkiv)
        Else
            bArkiv = True
            strConnectionString = AppSettings("ConnectionStringArchive")
            Dim i As Integer
            Dim strQuery2 As String
            For i = AppSettings("from") To AppSettings("to")
                '  (CreationDateTime < CONVERT(DATETIME, '1988-01-01', 102))
                strQuery2 = strQuery & " AND (CreationDateTime >= CONVERT(DATETIME, '" & i & ".11.10', 102)) AND (CreationDateTime <= CONVERT(DATETIME, '" & i & ".11.14', 102))"
                strQuery2 &= " ORDER BY RefID"
                MakeIdxFile(strConnectionString, strQuery2, fileIdx, "NTB_Arkiv_" & i, bArkiv)
            Next
        End If

    End Sub

    Private Sub MakeIdxFile(ByVal strConnectionString As String, ByVal strQuery As String, ByRef fileIdx As AutonomyIDX, ByVal strDbName As String, ByVal bArkiv As Boolean)
        Dim intArticleId As Integer
        Dim strXmlDoc As String

        Dim CreationDateTime As DateTime
        Dim ArticleTitle As String
        Dim Ingress As String

        Dim intNumRows As Integer

        Dim mySqlCommand As SqlCommand
        Me.SqlConnection1.ConnectionString = strConnectionString
        Me.SqlConnection1.Open()

        mySqlCommand = New SqlCommand()
        mySqlCommand.Connection = SqlConnection1
        mySqlCommand.CommandTimeout = 120
        mySqlCommand.CommandText = strQuery

        'Output
        Dim strIdxPath As String = AppSettings("autonomyIdxPath")
        Directory.CreateDirectory(strIdxPath)
        Dim strIdxFile As String = strIdxPath & "\Export_" & strDbName & ".idx"
        fileIdx.Open(strIdxFile)

        Dim dataReader As SqlDataReader
        dataReader = mySqlCommand.ExecuteReader()

        ' Always call Read before accessing data.
        While dataReader.Read()

            Try
                intArticleId = dataReader.GetInt32(0)
                strXmlDoc = dataReader.GetString(1)
                fileIdx.MakeIdx("NTB", intArticleId, strXmlDoc, bArkiv)
            Catch e As Exception
                'Hmmm..
            End Try

            intNumRows += 1
            If intNumRows Mod 100 = 0 Then
                Me.StatusBar1.Text = "Skriver rader: " & intNumRows.ToString
                Application.DoEvents()
                'Threading.Thread.Sleep(1000)
            End If
        End While

        'Me.SqlDataAdapter1.SelectCommand = mySqlCommand
        'Me.DataSet11.Clear()
        'Me.SqlDataAdapter1.Fill(Me.DataSet11)

        'Dim row As DataSet1.ARTICLESRow
        'For Each row In DataSet11.ARTICLES.Rows
        '    intArticleId = row.RefID 'myDataReader.GetInt32(0)

        '    strXmlDoc = row.ArticleXML  'myDataReader.GetString(4) & ""
        '    fileIdx.MakeIdx("NTB", intArticleId, strXmlDoc, bArkiv)

        '    intNumRows += 1
        '    If intNumRows Mod 100 = 0 Then
        '        Me.StatusBar1.Text = "Skriver rader: " & intNumRows.ToString
        '        Application.DoEvents()
        '    End If
        'Next
        'Me.DataSet11.Dispose()

        fileIdx.Close()

        'dataReader.Close()
        SqlConnection1.Close()
        Me.StatusBar1.Text = "Antall rader ferdig: " & intNumRows.ToString & " Til fila: " & strIdxFile

    End Sub

    Private Sub StartExport_old()
        Dim myDataReader As SqlDataReader
        Dim mySqlCommand As SqlCommand
        Dim intArticleId As Integer
        Dim CreationDateTime As DateTime
        Dim ArticleTitle As String
        Dim Ingress As String
        Dim intNumRows As Integer
        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim strSqlWhere As String
        Dim bArkiv As Boolean

        Dim strDbName As String = Me.ComboBox1.Text
        Dim strConnectionString As String

        'Dim strConnection = Me.SqlConnection1.ConnectionString
        strConnectionString = AppSettings("ConnectionString")
        Me.SqlConnection1.ConnectionString = strConnectionString

        Me.SqlConnection1.Open()

        SqlConnection1.ChangeDatabase(strDbName)

        strSqlWhere = AppSettings("SqlQueryWhere" & strDbName)

        Dim strQuery As String = "SELECT RefID, CreationDateTime, ArticleTitle, Ingress, ArticleXML FROM ARTICLES " & _
            strSqlWhere

        mySqlCommand = New SqlCommand(strQuery, SqlConnection1)
        myDataReader = mySqlCommand.ExecuteReader()

        Dim strIdxPath As String = AppSettings("autonomyIdxPath")
        Directory.CreateDirectory(strIdxPath)
        Dim strIdxFile As String = strIdxPath & "\NTB_Export_" & strDbName & ".idx"
        Dim fileIdx As AutonomyIDX = New AutonomyIDX(strConnectionString)
        fileIdx.Open(strIdxFile)
        'Dim sw As StreamWriter = New StreamWriter(strIdxFile, False, System.Text.Encoding.GetEncoding(1252))   ' create a stream writer 

        If strDbName = "NTB" Then
            bArkiv = False
        Else
            bArkiv = True
        End If

        While (myDataReader.Read())

            intArticleId = myDataReader.GetInt32(0)
            'CreationDateTime = myDataReader.GetDateTime(1)
            'ArticleTitle = myDataReader.GetString(2)
            'Ingress = myDataReader.GetString(3)
            'xmlDoc.LoadXml(myDataReader.GetString(4) & "")

            Dim strXmlDoc As String = myDataReader.GetString(4) & ""

            fileIdx.MakeIdx("NTB", intArticleId, strXmlDoc, bArkiv)

            intNumRows += 1
            If intNumRows Mod 100 = 0 Then
                Me.StatusBar1.Text = "Skriver rader: " & intNumRows.ToString
                Application.DoEvents()
            End If

        End While

        xmlDoc = Nothing
        fileIdx.Close()

        myDataReader.Close()
        Me.SqlConnection1.Close()
        Me.StatusBar1.Text = "Antall rader ferdig: " & intNumRows.ToString & " Til fila: " & strIdxFile
    End Sub

End Class
